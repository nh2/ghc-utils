#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Compute how many contributors have at least N commits for each year.
"""

from typing import List, Set, TypeVar, Dict
import subprocess
from collections import defaultdict

import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as pl

def commit_authors_during(since: str, until: str) -> List[str]:
    out = subprocess.check_output(['git', 'log', '--format=%an', f'--since={since}', f'--until={until}'], encoding='UTF-8')
    return list(out.split('\n'))

A = TypeVar('A')

def counts(xs: List[A]) -> Dict[A, int]:
    cs = defaultdict(lambda: 0)
    for x in xs:
        cs[x] += 1

    return cs

def main() -> None:
    cur_year = 2020

    commits_by_author_by_year = {
        (cur_year-years_ago): counts(commit_authors_during(f'{years_ago+1} years ago', f'{years_ago} years ago'))
        for years_ago in range(10)
    }

    # How many authors had more than N commits?
    more_than = lambda counted, n: len(list(author for author, commit_count in counted.items() if commit_count > n))

    out = [list(commits_by_author_by_year.keys())]
    for n in [0,2,5,10]:
        xs = [ (year, more_than(counted, n))
               for year, counted in commits_by_author_by_year.items()
             ]
        xs = np.array(xs)
        out.append(xs[:,1])
        pl.plot(xs[:,0], xs[:,1], label=f'{n} or more commits')

    print(np.array(out, dtype='i').T)
    pl.xlabel('year')
    pl.ylabel('number of contributors')
    pl.legend()
    pl.savefig('commit-counts.svg')

def pluralize(s: str, n: int):
    return s if n == 1 else f'{s}s'

if __name__ == '__main__':
    main()


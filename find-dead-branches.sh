#!/usr/bin/env bash

#
# Identify merged wip/* branches.
#

set -o pipefail

check_branch() {
  branch="$1"
  subj="$(git show --quiet --format="%s" "$branch")"
  master_commit="$(git log master --grep="$subj" --format=%H | head -n1)" || master_commit="dead"
  if [ -z "$master_commit" ]; then master_commit="dead"; fi
  echo "$master_commit $branch"
}

export -f check_branch
#for branch in $(git branch -r | grep origin/wip/); do check_branch $branch done
git branch -r | grep origin/wip/ | sed 's/^ *//'| parallel "check_branch {}"
